#include <iostream>
#include "String.h"
using namespace std;

void main()
{
	String my_string1("My string 1");
	my_string1.display();

	String my_string2 = my_string1;
	my_string2.display();

	String my_string3 = "My string 3";
	my_string3.display();

	String my_string4(my_string3);
	my_string4.display();

	system("pause");
}