#pragma once
#include <iostream>
#pragma warning (disable: 4996)

// Will develop other methods but now wonder why on some objects of String delete[] _str in dectr crashes

class String 
{
	int   _cnt;
	char *_str;

    public:
		String(): _cnt(0), _str("") {}
		String(char *str) : _cnt(strlen(str)), _str(strdup(str)){}
		String(String const *str) : _cnt(strlen(str->_str)), _str(strdup(str->_str)) {}
		~String() {}

		void display() { std::cout << _str << " \n"; };
};     